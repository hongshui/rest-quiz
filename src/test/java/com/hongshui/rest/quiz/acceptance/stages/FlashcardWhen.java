package com.hongshui.rest.quiz.acceptance.stages;

import com.tngtech.jgiven.Stage;

public class FlashcardWhen extends Stage<FlashcardWhen> {

  public FlashcardWhen triesToSaveFlashcard() {
    return this;
  }
}
