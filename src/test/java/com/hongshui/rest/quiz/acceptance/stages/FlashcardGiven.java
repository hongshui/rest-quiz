package com.hongshui.rest.quiz.acceptance.stages;

import com.tngtech.jgiven.Stage;

public class FlashcardGiven extends Stage<FlashcardGiven> {

  private static final String NEW_AND_VALID_QUESTION = "Question";
  private static final String NEW_AND_VALID_ANSWER = "Answer";
  private static final String NEW_AND_VALID_CATEGORY_NAME = "Category";

  public FlashcardGiven newFlashcard() {
    return this;
  }

  public FlashcardGiven fromNewCategory() {
    return this;
  }
}
