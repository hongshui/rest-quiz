package com.hongshui.rest.quiz.acceptance.stages;

import com.tngtech.jgiven.Stage;

public class FlashcardThen extends Stage<FlashcardThen> {

  public FlashcardThen questionIsSaved() {
    return this;
  }

  public FlashcardThen andCategoryIsSaved() {
    return this;
  }
}
