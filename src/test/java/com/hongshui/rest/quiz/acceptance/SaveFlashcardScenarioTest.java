package com.hongshui.rest.quiz.acceptance;

import com.hongshui.rest.quiz.acceptance.stages.FlashcardGiven;
import com.hongshui.rest.quiz.acceptance.stages.FlashcardThen;
import com.hongshui.rest.quiz.acceptance.stages.FlashcardWhen;
import com.tngtech.jgiven.integration.spring.EnableJGiven;
import com.tngtech.jgiven.integration.spring.junit5.SpringScenarioTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@EnableJGiven
@SpringBootTest
public class SaveFlashcardScenarioTest
    extends SpringScenarioTest<FlashcardGiven, FlashcardWhen, FlashcardThen> {

  //------------------------------------------------------------------------
  // BASIC
  //------------------------------------------------------------------------

  @Test
  public void shouldSaveBothFlashcardAndCategoryWhenNewQuestionFromNewCategoryIsProvided() {
    given()
        .newFlashcard()
        .fromNewCategory();

    when()
        .triesToSaveFlashcard();

    then()
        .questionIsSaved()
        .andCategoryIsSaved();
  }

  @Test
  public void shouldSaveOnlyQuestionWhenNewQuestionFromExistingCategoryIsProvided() {
  }

  //------------------------------------------------------------------------
  // DUPLICATES
  //------------------------------------------------------------------------

  @Test
  public void shouldNotAllowDuplicatedQuestionsCaseInsensitive() {
  }

  @Test
  public void shouldAllowDuplicatedAnswers() {
  }

  @Test
  public void shouldNotAllowDuplicatedCategoryNamesCaseInsensitive() {
  }

  //------------------------------------------------------------------------
  // UPDATES
  //------------------------------------------------------------------------

  @Test
  public void savingFunctionalityShouldNotAllowToUpdateQuestionExistingFlashcard() {
  }

  @Test
  public void savingFunctionalityShouldNotAllowToUpdateAnswerInExistingFlashcard() {
  }

  @Test
  public void savingFunctionalityShouldNotAllowToUpdateCategoryNameInExistingCategory() {
  }

  //------------------------------------------------------------------------
  // MISSING DATA
  //------------------------------------------------------------------------

  @Test
  public void shouldNotSaveFlashcardWhenQuestionIsMissing() {
  }

  @Test
  public void shouldNotSaveFlashcardWhenAnswerIsMissing() {
  }

  @Test
  public void shouldSaveFlashcardWhenCategoryIsMissing() {
  }

  //------------------------------------------------------------------------
  // ONLY SPACES
  //------------------------------------------------------------------------

  @Test
  public void shouldNotSaveFlashcardWhenQuestionContainsOnlySpaces() {
  }

  @Test
  public void shouldNotSaveFlashcardWhenAnswerContainsOnlySpaces() {
  }

  @Test
  public void shouldNotSaveFlashcardWhenCategoryNameContainsOnlySpaces() {
  }

  //------------------------------------------------------------------------
  // OVER 255 CHARACTERS
  //------------------------------------------------------------------------

  @Test
  public void shouldNotSaveFlashcardWhenQuestionIsLongerThan255Characters() {
  }

  @Test
  public void shouldNotSaveFlashcardWhenAnswerIsLongerThan255Characters() {
  }

  @Test
  public void shouldNotSaveFlashcardWhenCategoryNameIsLongerThan255Characters() {
  }

  //------------------------------------------------------------------------
  // TRAILING SPACES
  //------------------------------------------------------------------------

  @Test
  public void shouldRemoveTrailingSpacesFromQuestionBeforeSaving() {
  }

  @Test
  public void shouldRemoveTrailingSpacesFromAnswerBeforeSaving() {
  }

  @Test
  public void shouldRemoveTrailingSpacesFromCategoryNameSaving() {
  }
}