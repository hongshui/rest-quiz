package com.hongshui.rest.quiz.adapters.rest.exceptions;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
class ExceptionAdvice {

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  Set<FieldError> validationError(MethodArgumentNotValidException e) {
    return e.getBindingResult()
        .getFieldErrors()
        .stream()
        .map(f -> new FieldError(f.getField(), f.getDefaultMessage()))
        .collect(Collectors.toSet());
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.CONFLICT)
  @ExceptionHandler(ConstraintViolationException.class)
  String databaseError() {
    return "Data already exists in the database";
  }
}