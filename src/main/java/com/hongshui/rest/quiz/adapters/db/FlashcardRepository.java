package com.hongshui.rest.quiz.adapters.db;

import com.hongshui.rest.quiz.adapters.db.entity.FlashcardEntity;
import com.hongshui.rest.quiz.domain.model.basic.Flashcard;
import com.hongshui.rest.quiz.domain.ports.spi.FlashcardDbPort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlashcardRepository extends FlashcardDbPort, JpaRepository<FlashcardEntity, Long> {

  @Query("select f from Flashcard f where f.category.id in (:ids)")
  List<FlashcardEntity> findByCategoryIn(List<Long> ids);

  @Override
  default List<Flashcard> getAllFromCategory(List<Long> ids) {
    return null;
  }

  @Override
  default List<Flashcard> getAll() {
    return null;
  }

  @Override
  default void add(Flashcard flashcard) {
  }

  @Override
  default void update(Flashcard flashcard) {
  }

  @Override
  default void remove(Long ids) {
  }
}