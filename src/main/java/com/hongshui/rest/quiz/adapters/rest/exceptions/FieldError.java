package com.hongshui.rest.quiz.adapters.rest.exceptions;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
class FieldError {
  private final String field;
  private final String message;
}
