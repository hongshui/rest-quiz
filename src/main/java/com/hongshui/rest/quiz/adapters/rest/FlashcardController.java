package com.hongshui.rest.quiz.adapters.rest;

import com.hongshui.rest.quiz.domain.model.basic.Flashcard;
import com.hongshui.rest.quiz.domain.usecases.flashcard.SavingUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("flashcard")
@RequiredArgsConstructor
class FlashcardController {

  private final SavingUseCase service;

  @GetMapping("random")
  @ResponseStatus(HttpStatus.OK)
  Flashcard getQuestionByCategoryId(
      @RequestParam(value = "category", required = false) List<Long> categoryId) {
    return service.getQuestionBy(categoryId);
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  List<Flashcard> getAllQuestions(
      @RequestParam(value = "category", required = false) List<Long> categoryId) {
    return service.getAllQuestions(categoryId);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  String addQuestion(@RequestBody @Valid Flashcard flashCard) {
    service.add(flashCard);
    return "Flashcard added to the database";
  }

  @PutMapping
  @ResponseStatus(HttpStatus.OK)
  String updateQuestion(@RequestBody @Valid Flashcard flashCard) {
    service.update(flashCard);
    return "Flashcard updated to the database";
  }

  @DeleteMapping
  @ResponseStatus(HttpStatus.OK)
  String deleteQuestion(@RequestBody Long flashCardId) {
    service.delete(flashCardId);
    return "Flashcard deleted";
  }
}