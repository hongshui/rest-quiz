package com.hongshui.rest.quiz.domain.ports.spi;

import com.hongshui.rest.quiz.domain.model.basic.Flashcard;

import java.util.List;

public interface FlashcardDbPort {

  List<Flashcard> getAll();

  List<Flashcard> getAllFromCategory(List<Long> ids);

  List<Flashcard> getRandom();

  List<Flashcard> getRandomFromCategory();

  void add(Flashcard flashcard);

  void addMany(List<Flashcard> flashcards);

  void update(Flashcard flashcard);

  void updateMany(List<Flashcard> flashcards);

  void remove(Long id);

  void removeMany(List<Long> ids);
}
