package com.hongshui.rest.quiz.domain.model;

import lombok.RequiredArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RequiredArgsConstructor
public class ChangedFlashcard {

  private static final String FOR_QUESTION = "Question should be between 1 and 256 characters";
  private static final String FOR_ANSWER = "Answer should be between 1 and 256 characters";

  @NotNull
  @Min(value = 1, message = "Id cannot be less than 1")
  private final long id;

  @NotBlank(message = FOR_QUESTION)
  @Size(min = 1, max = 256, message = FOR_QUESTION)
  private final String question;

  @NotBlank(message = FOR_ANSWER)
  @Size(min = 1, max = 256, message = FOR_ANSWER)
  private final String answer;

  private final Long categoryId;
}
