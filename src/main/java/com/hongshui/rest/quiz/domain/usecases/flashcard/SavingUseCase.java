package com.hongshui.rest.quiz.domain.usecases.flashcard;

import com.hongshui.rest.quiz.domain.model.basic.Flashcard;
import com.hongshui.rest.quiz.domain.ports.spi.FlashcardDbPort;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class SavingUseCase {

  private final FlashcardDbPort repository;

  public Flashcard getQuestionBy(List<Long> categoryIds) {
    if (categoryIds == null) {
      return repository.getAll().stream().findAny().orElse(null);
    }
    return repository.getAllFromCategory(categoryIds).stream().findAny().orElse(null);
  }

  public List<Flashcard> getAllQuestions(List<Long> categoryIds) {
    if (categoryIds == null) {
      return repository.getAll();
    }
    return repository.getAllFromCategory(categoryIds);
  }

  public void add(Flashcard flashCard) {
    repository.add(flashCard);
  }

  public void update(Flashcard flashCard) {
    repository.update(flashCard);
  }

  public void delete(Long id) {
    repository.remove(id);
  }
}