package com.hongshui.rest.quiz.domain.model;

import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@RequiredArgsConstructor
public class NewCategory {

  @NotBlank(message = "Category cannot be empty")
  @Size(min = 1, max = 50, message = "Category should be between 1 and 50 characters")
  private final String name;
}
