package com.hongshui.rest.quiz.domain.model;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SavedCategory {
  private final long id;
  private final String name;
}
