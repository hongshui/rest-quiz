package com.hongshui.rest.quiz.domain.model.basic;

import com.hongshui.rest.quiz.adapters.db.entity.FlashcardEntity;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class Category {
  private final long id;
  private final String name;
  private final List<FlashcardEntity> flashcards;
}
