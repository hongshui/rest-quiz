package com.hongshui.rest.quiz.domain.model;

import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@RequiredArgsConstructor
public class NewFlashcard {

  private static final String FOR_QUESTION = "Question should be between 1 and 256 characters";
  private static final String FOR_ANSWER = "Answer should be between 1 and 256 characters";

  @NotBlank(message = FOR_QUESTION)
  @Size(min = 1, max = 256, message = FOR_QUESTION)
  private final String question;

  @NotBlank(message = FOR_ANSWER)
  @Size(min = 1, max = 256, message = FOR_ANSWER)
  private final String answer;

  private final Long categoryId;
}
