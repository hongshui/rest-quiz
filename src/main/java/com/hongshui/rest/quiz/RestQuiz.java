package com.hongshui.rest.quiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestQuiz {

  public static void main(String[] args) {
    SpringApplication.run(RestQuiz.class, args);
  }
}

