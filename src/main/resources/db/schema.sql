DROP TABLE flashcard IF EXISTS;
DROP TABLE category IF EXISTS;

CREATE TABLE category (
  id            BIGINT IDENTITY PRIMARY KEY,
  name          VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE flashcard (
  id            BIGINT IDENTITY PRIMARY KEY,
  question      VARCHAR(255) NOT NULL UNIQUE,
  answer        VARCHAR(255) NOT NULL,
  category_id   BIGINT
);

ALTER TABLE flashcard
ADD CONSTRAINT fk_category_id
FOREIGN KEY (category_id)
REFERENCES category (id);

